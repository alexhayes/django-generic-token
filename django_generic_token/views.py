from .models import Token
from django.views.generic.base import RedirectView, TemplateResponseMixin,\
    ContextMixin
from django.views.generic.detail import SingleObjectMixin, DetailView
from django_generic_token.models import TokenMaxRedemptionsError, TokenType,\
    Redemption
from django.core.exceptions import ImproperlyConfigured, ObjectDoesNotExist
from django.http.response import Http404
from django.shortcuts import redirect, get_object_or_404
from django.utils.translation import ugettext as _

class NoRedemptionInSessionError(Exception): pass

class TokenTypeMixin(ContextMixin):
    token_type = None
    token_type_slug = None
    
    def get_token_type(self):
        if self.token_type is not None:
            return self.token_type
        elif self.token_type_slug or self.request.GET.get('token_type_slug', False):
            slug = self.token_type_slug if self.token_type_slug is not None else self.request.GET.get('token_type_slug')
            try:
                self.token_type = TokenType.objects.get(slug=slug)
                return self.token_type
            except TokenType.DoesNotExist:
                raise Http404(_("Token Type with slug %(slug)s does not exist.") %
                              {'slug': self.request.GET.get('token_type_slug')})
        else:
            raise ImproperlyConfigured(
                "RedeemTokenView requires a definition of "
                "'token_type', 'get_token_type()' or get parameter 'token_type_slug'.")
            
class RedemptionMixin(TokenTypeMixin):
    """
    A mixin that aids the user in retrieving a redemption previously redeemed.
    
    Use in combination with the @token_required decorator.
    """

    def get_redemption(self):
        """
        Get a redemption from a request.
        
        :return: Redemption
        """
        try:
            token_type = self.get_token_type()
            key = token_type.get_session_key()
            return get_object_or_404(Redemption, 
                                     pk=self.request.session[key],
                                     token__token_type=token_type)
        except KeyError:
            raise NoRedemptionInSessionError('Request session does not contain session key: %s' % key)

class RedemptionDetailView(DetailView, RedemptionMixin):
    model = Redemption
    
    def get_object(self, queryset=None):
        return self.get_redemption()

class RedeemTokenView(SingleObjectMixin, TemplateResponseMixin, RedirectView, TokenTypeMixin):
    """
    Redeem a token and then redirect.
    
    You can either subclass this view adding a 'get_redirect_url()' method or you
    can pass in the GET parameter 'next' to redirect to.
    """
    permanent = False
    template_name = 'django_generic_token/token_max_redemptions_error.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        try:
            self.redemption = self.object.redeem()
            request.session[self.object.token_type.get_session_key()] = self.redemption.pk
            return super(RedeemTokenView, self).get(request, *args, **kwargs)
        except TokenMaxRedemptionsError:
            return self.render_to_response(self.get_context_data())
    
    def get_redirect_url(self, *args, **kwargs):
        if self.request.GET.get('next', False):
            return self.request.GET.get('next')

        raise ImproperlyConfigured(
                "RedeemTokenView requires a definition of "
                "'get_redirect_url()' or the GET parameter 'next' to be present.")

    def get_object(self, queryset=None):
        """
        Returns the Token object.

        By default this requires `self.queryset` and a `pk` or `slug` argument
        in the URLconf, but subclasses can override this to return any object.
        """
        try:
            # Get and return the token
            token_type = self.get_token_type()
            obj = token_type.tokens.get(token=self.kwargs.get('token'))
            return obj
        except Token.DoesNotExist:
            raise Http404(_("Token '%(token)s' does not exist.") %
                          {'token': self.request.GET.get('token')})
