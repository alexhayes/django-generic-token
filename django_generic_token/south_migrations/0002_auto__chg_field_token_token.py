# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Token.token'
        db.alter_column(u'django_generic_token_token', 'token', self.gf('django.db.models.fields.CharField')(unique=True, max_length=191))

    def backwards(self, orm):

        # Changing field 'Token.token'
        db.alter_column(u'django_generic_token_token', 'token', self.gf('django.db.models.fields.CharField')(max_length=255, unique=True))

    models = {
        u'django_generic_token.redemption': {
            'Meta': {'ordering': "('-verified',)", 'object_name': 'Redemption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'token': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'redemptions'", 'to': u"orm['django_generic_token.Token']"}),
            'verified': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'django_generic_token.token': {
            'Meta': {'ordering': "('-created',)", 'object_name': 'Token'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sent': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '191', 'db_index': 'True'}),
            'token_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tokens'", 'to': u"orm['django_generic_token.TokenType']"})
        },
        u'django_generic_token.tokentype': {
            'Meta': {'object_name': 'TokenType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_redemptions': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'token_length': ('django.db.models.fields.IntegerField', [], {'default': '16'})
        }
    }

    complete_apps = ['django_generic_token']