# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TokenType'
        db.create_table(u'django_generic_token_tokentype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('token_length', self.gf('django.db.models.fields.IntegerField')(default=16)),
            ('max_redemptions', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'django_generic_token', ['TokenType'])

        # Adding model 'Token'
        db.create_table(u'django_generic_token_token', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('token_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_generic_token.TokenType'])),
            ('token', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, db_index=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('sent', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'django_generic_token', ['Token'])

        # Adding model 'Redemption'
        db.create_table(u'django_generic_token_redemption', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('token', self.gf('django.db.models.fields.related.ForeignKey')(related_name='redemptions', to=orm['django_generic_token.Token'])),
            ('verified', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'django_generic_token', ['Redemption'])


    def backwards(self, orm):
        # Deleting model 'TokenType'
        db.delete_table(u'django_generic_token_tokentype')

        # Deleting model 'Token'
        db.delete_table(u'django_generic_token_token')

        # Deleting model 'Redemption'
        db.delete_table(u'django_generic_token_redemption')


    models = {
        u'django_generic_token.redemption': {
            'Meta': {'ordering': "('-verified',)", 'object_name': 'Redemption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'token': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'redemptions'", 'to': u"orm['django_generic_token.Token']"}),
            'verified': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'django_generic_token.token': {
            'Meta': {'ordering': "('-created',)", 'object_name': 'Token'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sent': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'token_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['django_generic_token.TokenType']"})
        },
        u'django_generic_token.tokentype': {
            'Meta': {'object_name': 'TokenType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_redemptions': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'token_length': ('django.db.models.fields.IntegerField', [], {'default': '16'})
        }
    }

    complete_apps = ['django_generic_token']