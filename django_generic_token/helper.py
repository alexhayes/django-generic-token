# When a Token is redeemed it's saved as a session var for later use.
SESSION_KEY = '%s_redemption'

def get_session_key(slug):
    return SESSION_KEY % slug.replace('-', '_')