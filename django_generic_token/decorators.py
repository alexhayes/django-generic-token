from django.core.urlresolvers import reverse
from functools import wraps
from django_generic_token.helper import get_session_key
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.core.exceptions import PermissionDenied

def token_required(token_type_slug, message="You must have a valid token to access this page."):
    """
    Decorate a view ensuring user has a token.

    Decorate your view as follows:

    ```python
    from django_generic_token.decorators import token_required
    
    @token_required('my-token-type-slug')
    def my_view(request):
    ```
    
    Or if using a class based view, in your urls.py:
    
    ```python
    from django_generic_token.decorators import token_required
    
    urlpatterns = patterns('',
        url(r'^somewhere/',
            view=token_required('my-token-type-slug')(MyClassBasedView.as_view()),
            name='myapp.somewhere'),
    )
    ```
    
    """
    def decorator(func):
        def inner_decorator(request, *args, **kwargs):
            session_key = get_session_key(token_type_slug)
            if session_key in request.session:
                return func(request, *args, **kwargs)
            else:
                raise PermissionDenied()
        return wraps(func)(inner_decorator)
    return decorator
