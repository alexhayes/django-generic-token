from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django_generic_token.models import TokenType, Token, Redemption

class TokenTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'token_length', 'max_redemptions')
    search_fieldsets = ('name',)
admin.site.register(TokenType, TokenTypeAdmin)

class TokenAdmin(admin.ModelAdmin):
    list_display = ('token', 'token_type', 'created', 'sent')
    list_filter = ('token_type',)
    search_fieldsets = ('token', 'created', 'sent')
admin.site.register(Token, TokenAdmin)

class RedemptionAdmin(admin.ModelAdmin):
    list_display = ('token', 'verified')
admin.site.register(Redemption, RedemptionAdmin)
    