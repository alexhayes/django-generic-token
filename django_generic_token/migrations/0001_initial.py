# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Redemption',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('verified', models.DateTimeField(help_text=b'The date the token was validated.', auto_now_add=True)),
            ],
            options={
                'ordering': ('-verified',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Token',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.CharField(help_text=b'The unique token that verifies the users address and allows them to download the file.', unique=True, max_length=191, db_index=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('sent', models.DateTimeField(help_text=b'The date this token was sent to the person/machine that should redeem it.', null=True, blank=True)),
            ],
            options={
                'ordering': ('-created',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TokenType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField()),
                ('token_length', models.IntegerField(default=16)),
                ('max_redemptions', models.IntegerField(help_text=b'The maximum number of redemptions allowed for this token', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='token',
            name='token_type',
            field=models.ForeignKey(related_name='tokens', to='django_generic_token.TokenType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='redemption',
            name='token',
            field=models.ForeignKey(related_name='redemptions', to='django_generic_token.Token'),
            preserve_default=True,
        ),
    ]
