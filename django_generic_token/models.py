from django.db import models
from django.utils.crypto import get_random_string
from .helper import get_session_key

class TokenMaxRedemptionsError(Exception): pass

class TokenType(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    token_length = models.IntegerField(default=16)
    max_redemptions = models.IntegerField(help_text='The maximum number of redemptions allowed for this token', null=True, blank=True)
    
    def __unicode__(self):
        return '%s' % (self.name)
    
    def get_session_key(self):
        return get_session_key(self.slug)
    
class Token(models.Model):
    """
    Represents a token that is used to verify a fan's email address.
    """
    token_type = models.ForeignKey(TokenType, related_name='tokens')
    token = models.CharField(null=False, max_length=191, help_text="The unique token that verifies the users address and allows them to download the file.", unique=True, db_index=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    sent = models.DateTimeField(help_text="The date this token was sent to the person/machine that should redeem it.", blank=True, null=True)

    class Meta:
        ordering = ('-created',)
        
    def __unicode__(self):
        return '%s' % (self.token)
    
    def save(self, *args, **kwargs):
        """
        Creates a token if one does not exist.
        """
        if not self.token:
            self.token = self.get_token()
        super(Token, self).save(*args, **kwargs)
    
    def get_token(self):
        """
        Creates a new unique token.
        """
        while True:
            token = get_random_string(self.token_type.token_length)
            try:
                Token.objects.get(token=token, token_type=self.token_type)
            except Token.DoesNotExist:
                return token

    def redeem(self):
        if self.token_type.max_redemptions is not None and self.redemptions.count() >= self.token_type.max_redemptions:
            raise TokenMaxRedemptionsError("Token '%s' already redeemed a maximum of %s times." % (self.token, self.token_type.max_redemptions))
        
        redemption = self.redemptions.create()
        
        return redemption

class Redemption(models.Model):
    token = models.ForeignKey(Token, related_name='redemptions')
    verified = models.DateTimeField(auto_now=False, auto_now_add=True, help_text="The date the token was validated.")
    
    class Meta:
        ordering = ('-verified',)
        
    def __unicode__(self):
        return '%s (%s)' % (self.token, self.verified)
    
