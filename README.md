# Django Generic Token

A reusable Django app that provides the generation, issuing and redemption of tokens.

Currently in alpha state.

## Installation

	pip install git+git://bitbucket.org/alexhayes/django-generic-token

Then add `django_generic_token` to your installed apps.

## Migration

Django Generic Token uses South for managing database migration, so:

	./manage.py migrate django_generic_token

## Generating Tokens

Firstly, you need to define a TokenType, on the django shell do the following:

```python
from django_generic_token.models import TokenType
token_type = TokenType.objects.create(name='Eggs', slug='eggs')
```

When defining a token type you can also set the following attributes:

- token_length: defaults to 16, defines the length of the generated token.
- max_redemptions: maximum number of redemptions allowed for this token.

Once this has been done you can create tokens as follows:

```python
token = Token.objects.create(token_type=token_type)
```

## Redeeming Tokens

### API

```
token = Token.objects.get(token_type=token_type, token='xyz...')
redemption = token.redeem()
```

Note that if you have defined `max_redemptions` when creating the TokenType you may need to catch `TokenMaxRedemptionsError`. 

### Urls

There is a `RedeemTokenView` that allows the redemption of tokens, thus you can define urls as follows:

```python
from django_generic_token.views import RedeemTokenView
from django.conf.urls import patterns, url
from django_generic_token.models import TokenType

urlpatterns = patterns('',
    url(r'^redeem/(?P<token_type_slug>[-\w]+)/(?P<token>\w+)/$', 
        view=RedeemTokenView.as_view(), 
        name='django_generic_token.redeem'),
    url(r'^redeem/(?P<token>\w+)/$', 
        view=RedeemTokenView.as_view(token_type=TokenType.objects.get(slug='xyz')), 
        name='django_generic_token.redeem'),
)
```

## Author

Alex Hayes <alex@alution.com>  